import java.util.Scanner;

public class Board {
	
	private String [][] table = new String[3][3];
	private Player x;
	private Player o;
	private Player winner;
	private Player current;
	private int turnCount;
	
	public Board(Player x, Player o) {
		this.x = x;
		this.o = o;
		current = x;
		turnCount = 0;
		winner = null;
		showTable();
	}
	
	public void showTable() {
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				table[i][j] = "-";
			}
		}
	}

	public String [][] getTable(){
		return table;
	}

	
	public Player getWinner() {
		return winner;
	}

	public void switchTurn() {
		if(current == x) {
			current = o;
		}else {
			current = x;
		}
		turnCount++;

	}
	
	public void printBoard() {
		System.out.println("   " + "1  " + " 2  " + " 3  ");

		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1 + "  ");
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(table[i][j] + "   ");
			}
			System.out.println();
		}
	}

	public void check() {
		Scanner kb = new Scanner(System.in);
		while (winner == null) {
			showTurn();
			int row = kb.nextInt();
			int col = kb.nextInt();
			if (OutBoard(row, col) == true) {
				System.out
						.println("You've inputed place, which is out of the board.\nPlease try again.");

			} else {
				if (isFull(row, col)) {
					System.err.println("Please try again.\n");

				} else {
					table[row - 1][col - 1] = getCurrent().getName();
					printBoard();
					if (Winner() == false) {
						if (current == x) {
							winner = x;
							System.out.println("The winner is "
									+ getCurrent().getName());
						} else {
							winner = o;
							System.out.println("The winner is "
									+ getCurrent().getName());
						}
					} else {
						changePlayer();
						turnCount++;
					}
				}

			}
			if (turnCount > 8) {
				System.out.println("Draw!!");
				break;
			}

		}
		if (getWinner() == x) {
			x.setWin();
			o.setLose();
		} else if (getWinner() == o) {
			o.setWin();
			x.setLose();
		} else {
			o.setDraw();
			x.setDraw();
		}

	}

	private Player getCurrent() {
		return current;
	}

	private Boolean Winner() {
		if (table[0][0] != "-" && table[0][0] == table[0][1]
				&& table[0][0] == table[0][2])
			return false;
		if (table[1][0] != "-" && table[1][0] == table[1][1]
				&& table[1][0] == table[1][2])
			return false;
		if (table[2][0] != "-" && table[2][0] == table[2][1]
				&& table[2][0] == table[2][2])
			return false;
		if (table[0][0] != "-" && table[0][0] == table[1][0]
				&& table[0][0] == table[2][0])
			return false;
		if (table[0][1] != "-" && table[0][1] == table[1][1]
				&& table[0][1] == table[2][1])
			return false;
		if (table[0][2] != "-" && table[0][2] == table[1][2]
				&& table[0][2] == table[2][2])
			return false;
		if (table[0][0] != "-" && table[0][0] == table[1][1]
				&& table[0][0] == table[2][2])
			return false;
		if (table[0][2] != "-" && table[0][2] == table[1][1]
				&& table[0][2] == table[2][0])
			return false;
		return true;
	}

	private void changePlayer() {
		if (current == x) {
			current = o;
		} else {
			current = x;
		}
	}

	private void showTurn() {
		System.out.println(current.getName() + " turn..");
		System.out.print("Please input row and column: ");
	}

	private boolean isFull(int row, int col) {
		if (table[row - 1][col - 1] == "X" || table[row - 1][col - 1] == "O") {
			return true;
		}
		return false;
	}

	private boolean OutBoard(int row, int col) {
		if (row > 3 || col > 3) {
			System.err.println("There is no row or column");
			return true;
		}
		return false;
	}
	
	
}
